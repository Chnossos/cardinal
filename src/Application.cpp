#include <Cardinal/Application.hpp>

// #include <Cardinal/MySQL/Database.hpp>
#include <Cardinal/Network/SessionManager.hpp>
#include <Cardinal/Network/SocketListener.hpp>
#include <boost/asio/signal_set.hpp>
#include <csignal>
#include <cstdlib>
#include <iostream>

int Application::run()
{
    std::cout << "Initializing Cardinal system..." << std::endl;

    addComponent<Network::SocketListener>("SocketListener", io_context);
    addComponent<Network::SessionManager>("SessionManager");
    // addComponent<Sql::Database>("Database");

    // Attempt to display non-ASCII characters correctly
    std::setlocale(LC_ALL, "");

    // Yep I'm using this
    std::srand(static_cast<unsigned int>(std::time(nullptr)));

    using namespace std::placeholders;
    boost::asio::signal_set signal_set { io_context, SIGINT };
    signal_set.async_wait(std::bind(&Application::onSignal, this, _1, _2));

    // if (!DATABASE.connect("cardinal", "127.0.0.1", 3306, "root", ""))
    // {
    //     auto const error = DATABASE.lastError();
    //     std::cerr << error << std::endl;
    //     return static_cast<int>(error.code);
    // }

    auto handler = std::bind(&Network::SessionManager::onNewSocketAccepted, &SESSION_MANAGER, _1);
    if (int const code = SOCKET_LISTENER.listen(4242, std::move(handler)))
        return code;

    io_context.get_executor().on_work_started();
    std::cout << "Server listening on *:4242, press CTRL+C to stop it..." << std::endl;
    io_context.run();

    return 0;
}

void Application::shutdown()
{
    SOCKET_LISTENER.shutdown();
    SESSION_MANAGER.shutdown();
}

void Application::onSignal(boost::system::error_code const & error, int signal)
{
    switch (auto const code = error.default_error_condition().value())
    {
        case boost::system::errc::success:
            handleSignal(signal);
            break;

        case boost::system::errc::operation_canceled:
            std::cerr << "SIGINT listener was canceled, stopping the world..." << std::endl;
            break;

        default:
            std::cerr << "SIGINT listener canceled due to unknown error code "
                      << code << ": " << error.message()
                      << "\nStopping the world..." << std::endl;
            break;
    }
    shutdown();
    io_context.get_executor().on_work_finished();
}

void Application::handleSignal(int signal)
{
    switch (signal)
    {
        case SIGINT:
            std::cout << "Caught SIGINT signal, stopping the world..." << std::endl;
            break;

        default:
            std::cerr << "Caught unknown signal "  << signal
                      << ", stopping the world..." << std::endl;
            break;
    }
    // Restore default signal action to be able to terminate if stuck
    std::signal(signal, SIG_DFL);
}
