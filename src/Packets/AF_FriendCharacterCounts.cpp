#include <Cardinal/Packets/AF_FriendCharacterCounts.hpp>

using Network::Packet::AF;

std::ostream & AF::CharacterDistribution::serialize(std::ostream & writer) const
{
    return Utility::join(writer, ',', serverId, characterCount);
}

void AF::serialize(std::ostream & writer) const
{
    Utility::join(writer, ';',
        std::cbegin(characterDistribution), std::cend(characterDistribution));
}
