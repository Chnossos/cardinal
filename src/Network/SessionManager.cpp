#include <Cardinal/Network/SessionManager.hpp>

#include <Cardinal/Application.hpp>
#include <Cardinal/Components/Account.hpp>
#include <iostream>

using namespace Network;

void SessionManager::shutdown()
{
    for (auto && it : _sessions)
    {
#ifdef NDEBUG
        // We want that sweet auto-reconnect popup in debug mode <3
        it.second->send("k");
        // Sending is async, don't shut down too early
        APP.io_context.post([s = it.second] { s->shutdown(); });
#else
        it.second->shutdown();
#endif
    }
}

void SessionManager::markSessionAsPending(Session::Id id, std::string_view connectionTicket)
{
    _pendingSessions.emplace(connectionTicket, id);
    // TODO: Erase it if client does not reconnect in a few seconds
}

bool SessionManager::mergeSessionWithPending(Session & tmpSession, std::string_view ticket)
{
    auto const it = _pendingSessions.find(ticket);
    if (it == _pendingSessions.end()) return false;

    _sessions.at(it->second)->resumeFrom(tmpSession);
    _sessions.erase(tmpSession.id());
    _pendingSessions.erase(ticket);
    return true;
}

void SessionManager::removeSession(Session::Id id)
{
    auto it = _sessions.find(id);
    if (it != _sessions.end())
    {
        _pendingSessions.erase(ACCOUNT(*it->second).connectionTicket);
        APP.io_context.post([s = _sessions.at(id)] { s->shutdown(); });
        _sessions.erase(id);
    }
}

void SessionManager::onNewSocketAccepted(boost::asio::ip::tcp::socket && socket)
{
    static Session::Id id = 0;

    auto session = std::make_shared<Session>(id, std::move(socket));
    std::cout << "New session created (id: '" << id << "')" << std::endl;
    session->start();

    _sessions.emplace(id++, session);
}
