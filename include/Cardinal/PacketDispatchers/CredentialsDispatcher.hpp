#pragma once

#include <Cardinal/Components/PacketDispatcher.hpp>

namespace PacketDispatchers
{
    struct CredentialsDispatcher : public Components::PacketDispatcher<CredentialsDispatcher>
    {
        bool dispatch(std::string_view packet, Network::Session::Ptr const & s) final;
    };
} // !namespace PacketDispatchers
