#pragma once

#include <Cardinal/Network/IPacket.hpp>
#include <string>

namespace Network::Packet
{
    struct Password : public BasicPacket<Password>
    {
        std::string password;
        Password(std::string_view data)
            : password { data }
        {}
    };
} // !namespace Network::Packet

INIT_PACKET_CUSTOM_HEADER(Password, "#1");
