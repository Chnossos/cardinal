#pragma once

#include <Cardinal/Components/PacketDispatcher.hpp>

namespace PacketDispatchers
{
    struct TicketDispatcher : public Components::PacketDispatcher<TicketDispatcher>
    {
        bool dispatch(std::string_view packet, Network::Session::Ptr const & s) final;
    };
} // !namespace PacketDispatchers

#define TICKET_DISPATCHER_FACTORY(P) DEFAULT_FACTORY(TicketDispatcher, P)
#define TICKET_DISPATCHER_HANDLER(P) DEFAULT_HANDLER(TicketDispatcher, P)
