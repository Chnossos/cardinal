#pragma once

#include <memory>

namespace Utility
{
    template<typename T>
    class Singleton
    {
    private:
        static std::unique_ptr<T> _instance;

    protected:
        Singleton() = default;

    public:
        Singleton(Singleton const &) = delete;
        Singleton(Singleton &&) = delete;
        Singleton & operator=(Singleton const &) = delete;
        Singleton & operator=(Singleton &&) = delete;

    public:
        template<typename... Args>
        static void init(Args &&... args) {
            _instance.reset(new T { std::forward<Args>(args)... });
        }

        static auto instance() -> T & {
            return *_instance;
        }

        static void deinit() {
            _instance.reset();
        }
    };

    template<typename T> std::unique_ptr<T> Singleton<T>::_instance = nullptr;
} // !namespace Utility
