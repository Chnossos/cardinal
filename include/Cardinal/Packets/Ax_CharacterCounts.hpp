#pragma once

#include <Cardinal/Network/IPacket.hpp>
#include <Cardinal/Network/Session.hpp>
#include <ctime>
#include <unordered_map>

namespace Network::Packet
{
    class Ax : public SuccessPacket<Ax>
    {
    public:
        std::time_t                  const remainingSubscriptionTime = -1;
        std::unordered_map<int, int> const characterAmountPerServer; //!< <serverId, amount>

    public:
        explicit Ax(std::string_view const &) {}
        Ax(int characterAmount, Network::Session const & session);

    private:
        void serialize(std::ostream & writer) const override;
    };
} // !namespace Network::Packet

INIT_PACKET_HEADER(Ax);
