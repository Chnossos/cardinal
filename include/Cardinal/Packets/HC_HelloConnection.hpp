#pragma once

#include <Cardinal/Components/Account.hpp>
#include <Cardinal/Network/IPacket.hpp>

namespace Network::Packet
{
    class HC : public BasicPacket<HC>
    {
    public:
        std::string const & connectionKey;

    public:
        explicit HC(Components::Account const & account)
            : connectionKey { account.connectionKey }
        {}

    private:
        void serialize(std::ostream & writer) const override {
            writer << connectionKey;
        }
    };
} // !namespace Network::Packet

INIT_PACKET_HEADER(HC);
