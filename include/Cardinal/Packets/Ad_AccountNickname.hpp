#pragma once

#include <Cardinal/Network/IPacket.hpp>

namespace Network::Packet
{
    struct Ad : public BasicPacket<Ad>
    {
        std::string const & nickname;
        explicit Ad(std::string const & nickname_) : nickname { nickname_ } {}

    private:
        void serialize(std::ostream & writer) const override {
            writer << nickname;
        }
    };
} // !namespace Network::Packet

INIT_PACKET_HEADER(Ad);
