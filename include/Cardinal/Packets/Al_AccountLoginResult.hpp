#pragma once

#include <Cardinal/Network/IPacket.hpp>
#include <Cardinal/Utility/Join.hpp>
#include <ctime>

namespace Network::Packet
{
    class Al : public SuccessPacket<Al>
    {
    public:
        enum Error : char
        {
            UNSPECIFIED,
            ACCESS_DENIED              = 'f',
            ALREADY_LOGGED             = 'a',
            ALREADY_LOGGED_GAME_SERVER = 'c',
            BAD_VERSION                = 'v',
            BANNED                     = 'b',
            CONNECT_NOT_FINISHED       = 'n',
            KICKED                     = 'k',
            MAINTAIN_ACCOUNT           = 'm',
            NICKNAME_MISSING           = 'r',
            NICKNAME_ALREADY_USED      = 's',
            NOT_PLAYER                 = 'p',
            OLD_ACCOUNT                = 'o',
            OLD_ACCOUNT_USE_NEW        = 'e',
            SERVER_FULL                = 'w',
            U_DISCONNECT_ACCOUNT       = 'd',
        };

    public:
        bool const isAdmin;

    public:
        explicit Al(bool isAdmin_) : isAdmin { isAdmin_ } {}

    private:
        void serialize(std::ostream & writer) const override {
            writer << isAdmin;
        }
    };

    DECLARE_BASE_ERROR_PACKET(Al) {};

    DECLARE_ERROR_PACKET(Al, Al::Error::BAD_VERSION)
    {
        std::string_view const version;
        explicit AlE(std::string_view version_) : version { version_ } {}
        void serialize(std::ostream & writer) const override {
            writer << version;
        }
    };

    DECLARE_ERROR_PACKET(Al, Al::Error::KICKED)
    {
        std::time_t const remainingBannedTime;
        explicit AlE(std::time_t remainingBannedTime_) : remainingBannedTime { remainingBannedTime_ } {}
        void serialize(std::ostream & writer) const override
        {
                auto       seconds = remainingBannedTime;
                auto const minutes = (seconds /= 60) % 60;
                auto const hours   = (seconds /= 60) % 24;
                auto const days    = (seconds /  24);
                Utility::join(writer, '|', days, hours, minutes);
        }
    };
} // !namespace Network::Packet

INIT_PACKET_HEADER(Al);
