SRCDIR := src
BLDDIR := build
BINDIR := bin

TARGET := $(BINDIR)/cardinal
FOLDER := $(patsubst $(SRCDIR)%, $(BLDDIR)%, $(shell find $(SRCDIR) -type d)) $(BINDIR)
SOURCE := $(shell find $(SRCDIR) -type f -name "*.cpp")
OBJECT := $(SOURCE:$(SRCDIR)/%.cpp=$(BLDDIR)/%.o)

BOOST_ROOT := C:\CPP\boost

CPPFLAGS := -MMD -MP -Iinclude
CXXFLAGS := -std=c++2a -pedantic -Wall -Wextra
LDFLAGS  :=
LDLIBS   :=

ifeq "$(OS)" "Windows_NT"
TARGET   := $(TARGET).exe
CPPFLAGS += -I$(BOOST_ROOT)/include/boost-1_73
LDFLAGS  += -L$(BOOST_ROOT)/lib
LDLIBS   += -lws2_32 -lwsock32 -lboost_system-mgw8-mt-d-x64-1_73
else
CPPFLAGS += -pthread
LDLIBS   += -lpthread -lboost_system
endif

## ############################################################################

.PHONY: all clean fclean re cmake

all: $(TARGET)

clean:
	$(RM) -r $(BLDDIR)

fclean: clean
	$(RM) -r $(BINDIR)

re: fclean
	@+$(MAKE) --no-print-directory all

cmake: BLDDIR:=$(BLDDIR)/Debug
cmake:
	conan install -if $(BLDDIR) -pr=mingw64-debug .
	cmake -S . -B $(BLDDIR) -G "Ninja" -DCMAKE_BUILD_TYPE=Debug
	cmake --build $(BLDDIR) -j $$(nproc)

## ############################################################################

$(TARGET): $(OBJECT) | $(BINDIR)
	@echo "Linking $@"
	@$(CXX) $(LDFLAGS) $^ $(LDLIBS) -o $@

.SECONDEXPANSION:
$(BLDDIR)/%.o: $(SRCDIR)/%.cpp | $$(@D)
	@echo "Compiling $<"
	@$(CXX) $(CPPFLAGS) $(CXXFLAGS) -o $@ -c $<

$(FOLDER):
	mkdir -p $@

## ############################################################################

-include $(OBJECT:.o=.d)
