#pragma once

#include <Cardinal/Network/IPacket.hpp>

namespace Network::Packet
{
    struct Ac : public BasicPacket<Ac>
    {
        enum class Community : int
        {
            FRENCH_SPEAKING  =  0,
            UK_AND_IRELAND   =  1,
            INTERNATIONAL    =  2,
            GERMAN           =  3,
            SPANISH_SPEAKING =  4,
            RUSSIAN          =  5,
            BRAZILIAN        =  6,
            DUTCH            =  7,
            ITALIAN          =  9,
            JAPANESE         = 10,
            DEBUG            = 99
        };

    public:
        Community const community;
        explicit Ac(Community community_) : community { community_ } {}

    private:
        void serialize(std::ostream & writer) const override {
            writer << static_cast<std::underlying_type_t<Community>>(community);
        }
    };
} // !namespace Network::Packet

INIT_PACKET_HEADER(Ac);
