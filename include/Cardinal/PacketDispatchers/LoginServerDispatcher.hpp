#pragma once

#include <Cardinal/Components/PacketDispatcher.hpp>

namespace PacketDispatchers
{
    struct LoginServerDispatcher : public Components::PacketDispatcher<LoginServerDispatcher>
    {
        bool dispatch(std::string_view packet, Network::Session::Ptr const & s) final;
    };
} // !namespace PacketDispatchers

#define LOGIN_SERVER_DISPATCHER_FACTORY(P)    DEFAULT_FACTORY(LoginServerDispatcher, P)
#define LOGIN_SERVER_DISPATCHER_HANDLER(P)    DEFAULT_HANDLER(LoginServerDispatcher, P)
#define DEFINE_LOGIN_SERVER_PACKET_HANDLER(P) DEFINE_PACKET_HANDLER(LoginServerDispatcher, P)
