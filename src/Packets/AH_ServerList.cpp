#include <Cardinal/Packets/AH_ServerList.hpp>

#include <Cardinal/Utility/Join.hpp>

using Network::Packet::AH;

std::ostream & AH::ServerData::serialize(std::ostream & writer) const
{
    return Utility::join(writer, ';',
        id, status, autoSelectPriority, canBeAutoSelected);
}

void AH::serialize(std::ostream & writer) const
{
    Utility::join(writer, '|', std::cbegin(servers), std::cend(servers));
}
