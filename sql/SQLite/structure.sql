DROP TABLE IF EXISTS accounts;
CREATE TABLE accounts
(
    id                    INTEGER PRIMARY KEY,
    name                  TEXT    NOT NULL UNIQUE,
    password              TEXT    NOT NULL,
    nickname              TEXT    NOT NULL UNIQUE,
    access_level          INTEGER NOT NULL DEFAULT 0,
    question              TEXT    NOT NULL,
    answer                TEXT    NOT NULL,
    creation_date         INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP,
    subscription_end_date INTEGER,
    banishment_end_date   INTEGER,
    last_connection_date  INTEGER,
    last_ip               TEXT
);

INSERT INTO accounts (name, password, nickname, access_level, question, answer) VALUES
('Admin', 'admin', 'Administrator', 1, 'DELETE?', 'DELETE'); /* 21232f297a57a5a743894a0e4a801fc3 */