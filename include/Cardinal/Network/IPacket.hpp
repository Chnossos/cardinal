#pragma once

#include <array>
#include <sstream>
#include <string_view>

namespace Network
{
    class IPacket
    {
    public:
        IPacket() = default;
        IPacket(IPacket &&) = default;
        IPacket & operator=(IPacket &&) = default;
        virtual ~IPacket() = 0;

    public:
        virtual std::string serialize() const = 0;
    };

    inline IPacket::~IPacket() = default;

namespace details
{
    template<typename T>
    class PacketImpl : public IPacket
    {
    public:
        static char const * const header;

    public:
        std::string serialize() const override
        {
            std::ostringstream writer { header, std::ios::ate };
            serialize(writer);
            return writer.str();
        }

    protected:
        virtual void serialize([[maybe_unused]] std::ostream & writer) const {}
    };

    template<typename T, bool, char c = 0>
    class BooleanPacket;

    template<typename T, char c>
    class BooleanPacket<T, true, c> : public PacketImpl<T>
    {
        using PacketImpl<T>::serialize;
        std::string serialize() const override
        {
            std::ostringstream writer { PacketImpl<T>::header, std::ios::ate };
            serialize(writer << 'K');
            return writer.str();
        }
    };

    template<typename T, char c>
    class BooleanPacket<T, false, c> : public PacketImpl<T>
    {
        using PacketImpl<T>::serialize;
        std::string serialize() const override
        {
            std::ostringstream writer { PacketImpl<T>::header, std::ios::ate };
            writer << 'E';
            if constexpr (c) writer << c;
            serialize(writer);
            return writer.str();
        }
    };
} // !namespace details

    template<typename T> using BasicPacket   = details::PacketImpl<T>;
    template<typename T> using SuccessPacket = details::BooleanPacket<T, true>;
    template<typename T> using ErrorPacket   = details::BooleanPacket<T, false>;
} // !namespace Network

#define DECLARE_BASE_ERROR_PACKET(P) \
    template<char c> \
    struct P ## E : public Network::details::BooleanPacket<P, false, c>
#define DECLARE_ERROR_PACKET(P, C) \
    template<> \
    struct P ## E<C> : public Network::details::BooleanPacket<P, false, C>

#define INIT_PACKET_CUSTOM_HEADER(P, H) \
    template<> \
    constexpr char const * const Network::details::PacketImpl<Network::Packet::P>::header = H
#define INIT_PACKET_HEADER(P) INIT_PACKET_CUSTOM_HEADER(P, #P)
