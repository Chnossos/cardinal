#include <Cardinal/Application.hpp>
#include <exception>
#include <iostream>

int main(int argc, char * argv[]) try
{
    Application::init(argc, argv);
    auto const code = Application::instance().run();
    Application::deinit();

    return code;
}
catch (std::exception const & e)
{
    std::cerr << "Fatal exception occurred: " << e.what() << std::endl;
    throw ;
}
catch (...)
{
    std::cerr << "Fatal uncaught exception occurred, terminating." << std::endl;
    throw ;
}
