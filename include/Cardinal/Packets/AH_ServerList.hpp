#pragma once

#include <Cardinal/Network/IPacket.hpp>
#include <Cardinal/Utility/Serializable.hpp>
#include <vector>

namespace Network::Packet
{
    class AH : public BasicPacket<AH>
    {
    private:
        struct ServerData : public Utility::Serializable
        {
            int  const id, status, autoSelectPriority;
            bool const canBeAutoSelected;

            ServerData(int id_, int status_, int autoSelectPriority_, bool canBeAutoSelected_)
                : id                 { id_                 }
                , status             { status_             }
                , autoSelectPriority { autoSelectPriority_ }
                , canBeAutoSelected  { canBeAutoSelected_  }
            {}
            std::ostream & serialize(std::ostream & writer) const override;
        };

    public:
        using ServerDataVector = std::vector<ServerData>;
        ServerDataVector const & servers;

    public:
        explicit AH(ServerDataVector const & servers_) : servers { servers_ } {}

    private:
        void serialize(std::ostream & writer) const override;
    };
} // !namespace Network::Packet

INIT_PACKET_HEADER(AH);
