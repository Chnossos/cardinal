#pragma once

namespace Utility
{
class Component
{
protected:
    Component() = default;

public:
    Component(Component const &) = delete;
    Component(Component &&) = delete;
    Component & operator=(Component const &) = delete;
    Component & operator=(Component &&) = delete;
    virtual ~Component() = default;
};
} // !namespace Utility
