#include <Cardinal/PacketDispatchers/TicketDispatcher.hpp>

#include <Cardinal/PacketDispatchers/VersionDispatcher.hpp>
#include <Cardinal/Network/SessionManager.hpp>
#include <Cardinal/Packets/AT_AccountTicket.hpp>
#include <iostream>

using namespace PacketDispatchers;
using namespace Network::Packet;

DEFINE_PACKET_FACTORY_MAP(TicketDispatcher,
    TICKET_DISPATCHER_FACTORY(AT)
);

DEFINE_PACKET_HANDLER_MAP(TicketDispatcher,
    TICKET_DISPATCHER_HANDLER(AT)
);

bool TicketDispatcher::dispatch(std::string_view packet, Network::Session::Ptr const & session)
{
    if (packet.size() < 2)
    {
        std::cerr << "Error: Packet is too small (content: <" << packet << ">). "
                  << "Potential packet forgery or non-retail client!" << std::endl;
    }
    else if (packet.compare(0, 2, AT::header) != 0)
    {
        std::cerr << "Error: Unexpected packet header <" << packet.substr(0, 2) << ">. "
                  << "Potential packet forgery or non-retail client!" << std::endl;
    }
    else
    {
        auto const packetPtr = _packetFactory.at(AT::header)(packet.substr(2));
        if (_packetHandlers.at(AT::header)(*packetPtr, session))
        {
            session->replaceComponent<VersionDispatcher>("PacketDispatcher");
            session->read();
            return true;
        }
    }
    SESSION_MANAGER.removeSession(session->id());
    return false;
}
