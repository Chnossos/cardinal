#pragma once

#include <Cardinal/Network/IPacket.hpp>
#include <Cardinal/Network/Session.hpp>

namespace Network::Packet
{
    class Af : public BasicPacket<Af>
    {
    public:
        int  const queueId          = -1, position = 1;
        int  const totalSubscribers = 0,  totalNonSubscribers = 1;
        bool const isSubscriber = false;

    public:
        explicit Af(std::string_view const &) {}
        explicit Af(Network::Session const &) {}

    public:
        void serialize(std::ostream & writer) const override;
    };
} // !namespace Network::Packet

INIT_PACKET_HEADER(Af);
