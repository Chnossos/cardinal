#include <Cardinal/Network/SocketListener.hpp>
#include <iostream>

using namespace Network;
using tcp = boost::asio::ip::tcp;

SocketListener::SocketListener(boost::asio::io_context & io_context)
    : _io_context { io_context }
{}

int SocketListener::listen(uint16_t port, Network::SocketListener::AcceptCallback cb)
{
    if (!_acceptors.emplace(port, _io_context).second)
    {
        std::cerr << "[WARNING] This SocketListener already listens to *:" << port << std::endl;
        return 0;
    }

    tcp::endpoint const endpoint { tcp::v4(), port };
    boost::system::error_code ec;

    if (!_acceptors.at(port).open(endpoint.protocol(), ec)                         &&
        !_acceptors.at(port).set_option(tcp::acceptor::reuse_address { true }, ec) &&
        !_acceptors.at(port).bind(endpoint, ec)                                    &&
        !_acceptors.at(port).listen(boost::asio::socket_base::max_listen_connections, ec))
    {
        using namespace std::placeholders;
        _callbacks.emplace(port, std::move(cb));
        _acceptors.at(port).async_accept(std::bind(&SocketListener::onAccept, this, port, _1, _2));
    }
    else
        _acceptors.erase(port);

    return handleError(port, ec);
}

void SocketListener::shutdown()
{
    boost::system::error_code error;
    for (auto && it : _acceptors)
        handleError(it.first, it.second.cancel(error));

    _acceptors.clear();
}

void SocketListener::onAccept(uint16_t port,
                              boost::system::error_code const & error,
                              boost::asio::ip::tcp::socket socket)
{
    if (handleError(port, error)) return ;

    using namespace std::placeholders;
    std::invoke(_callbacks.at(port), std::move(socket));
    _acceptors.at(port).async_accept(std::bind(&SocketListener::onAccept, this, port, _1, _2));
}

int SocketListener::handleError(uint16_t port, boost::system::error_code const & ec)
{
    auto const code = ec.default_error_condition().value();
    switch (code)
    {
        case boost::system::errc::success:
            break;

        case boost::system::errc::operation_canceled:
            std::cout << "SocketListener (port: " << port
                      << ") shut down successfully" << std::endl;
            break;

        default:
            std::cerr << "SocketListener (port: " << port << ") error " << code << ": "
                      << ec.message() << std::endl;
            break;
    }
    return code;
}

