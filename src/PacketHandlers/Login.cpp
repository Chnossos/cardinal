#include <Cardinal/Application.hpp>
#include <Cardinal/Components/Account.hpp>
#include <Cardinal/PacketDispatchers/CredentialsDispatcher.hpp>
#include <Cardinal/PacketDispatchers/LoginServerDispatcher.hpp>
#include <Cardinal/PacketDispatchers/TicketDispatcher.hpp>
// #include <Cardinal/MySQL/Database.hpp>
#include <Cardinal/Network/SessionManager.hpp>
#include <Cardinal/Packets/Ac_AccountCommunity.hpp>
#include <Cardinal/Packets/Al_AccountLoginResult.hpp>
#include <Cardinal/Packets/Ad_AccountNickname.hpp>
#include <Cardinal/Packets/AQ_AccountQuestion.hpp>
#include <Cardinal/Packets/AT_AccountTicket.hpp>
#include <Cardinal/Packets/Ax_CharacterCounts.hpp>
#include <Cardinal/Packets/AF_FriendCharacterCounts.hpp>
#include <Cardinal/Packets/HC_HelloConnection.hpp>
#include <Cardinal/Packets/Password.hpp>
#include <Cardinal/Packets/AH_ServerList.hpp>
#include <Cardinal/Packets/AXY_ServerSelection.hpp>
#include <Cardinal/Packets/Af_WaitingQueue.hpp>
#include <cstdlib>
#include <iostream>
#include <regex>

using namespace Network::Packet;

DEFINE_PACKET_HANDLER(TicketDispatcher, AT)
{
    using namespace std::string_view_literals;
    static constexpr auto const alphabet = "abcdefghijklmnopqrstuvwxyz"sv;

    if (packet.connectionKey == "undefined")
    {
        auto & account = session->addComponent<Components::Account>("Account");

        std::size_t size = 32;
        account.connectionKey.resize(size);
        while (size --> 0)
            account.connectionKey[size] = alphabet[static_cast<size_t>(std::rand()) % alphabet.size()];

        SEND_PACKET(*session, HC, account);
        return true;
    }
    else
    {
        if (!SESSION_MANAGER.mergeSessionWithPending(*session, packet.connectionKey))
            session->send("M031"); // Unknown connectionTicket

        return false;
    }
}

DEFINE_PACKET_HANDLER(CredentialsDispatcher, Password)
{
    if (packet.password.empty())
    {
        std::cerr << "Error: Empty password packet (id: '" << session->id()
                  << "'). Potential packet forgery or non-retail client!" << std::endl;
        SEND_PACKET_ERROR(*session, Al, Al::Error::ACCESS_DENIED);
        return false;
    }

    auto & account = ACCOUNT(*session);
    // Password hash is generated from the connection key (see loader.ank.utils.Crypt)
    auto const maxPwdSize = account.connectionKey.size();
    if (packet.password.size() > maxPwdSize * 2)
    {
        std::cerr << "Error: Password is too long, should be less or equal than "
                  << maxPwdSize << " characters (id: '" << session->id() << "'). "
                  << "Please review account registration policy." << std::endl;
        // Player should change password or reach out to the staff to do it
        SEND_PACKET_ERROR(*session, Al, Al::Error::MAINTAIN_ACCOUNT);
        return false;
    }
        // 2 characters are generated from each one of the client input, this packet is forged
    else if (packet.password.size() % 2 != 0)
    {
        std::cerr << "Error: Password packet is incomplete (id: '" << session->id()
                  << "'). Potential packet forgery!" << std::endl;
        SEND_PACKET_ERROR(*session, Al, Al::Error::ACCESS_DENIED);
        return false;
    }

    using namespace std::string_view_literals;
    static constexpr auto const hash =
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_"sv;

    if (std::any_of(packet.password.cbegin(), packet.password.cend(), [] (char c) {
        return hash.find(c) == hash.npos;
    }))
    {
        std::cerr << "Error: Unknown data encountered in password packet (id: '" << session->id()
                  << "'). Potential packet forgery or non-retail client!" << std::endl;
        SEND_PACKET_ERROR(*session, Al, Al::Error::ACCESS_DENIED);
        return false;
    }

    account.password.resize(packet.password.size() / 2);
    for (size_t i = 0, j = 0 ; i < packet.password.size() ; i += 2, ++j)
    {
        int part1 = static_cast<int>(hash.find(packet.password[i])) - account.connectionKey[j];
        while (part1 < 0) part1 += hash.size();
        part1 *= 16;

        int part2 = static_cast<int>(hash.find(packet.password[i + 1])) - account.connectionKey[j];
        while (part2 < 0) part2 += hash.size();

        account.password[j] = static_cast<char>(part1 + part2);
    }

    return true;
}

DEFINE_LOGIN_SERVER_PACKET_HANDLER(Af)
{
    auto & account    = ACCOUNT(*session);
    // auto const result = DATABASE.query(
    //     "SELECT"
    //     "    nickname, access_level, question, answer,"
    //     "    UNIX_TIMESTAMP(subscription_end_date), UNIX_TIMESTAMP(banishment_end_date),"
    //     "    UNIX_TIMESTAMP(last_connection_date), last_ip"
    //     " FROM account"
    //     " WHERE name = '" + account.username + "' AND password = '" + account.password + "' LIMIT 1"
    // );

    // if (!result)
    // {
    //     std::cerr << DATABASE.lastError() << std::endl;
    //     return false;
    // }
    // else if (result->isEmpty())
    // {
    //     SEND_PACKET_ERROR(*session, Al, Al::Error::ACCESS_DENIED);
    //     return false;
    // }

    // (*result)[0] >> account;

    // DATABASE.query(
    //     "UPDATE account SET"
    //     " last_connection_date = CURRENT_TIMESTAMP,"
    //     " last_ip              = '" + session->ip() + "' "
    //     "WHERE name = '" + account.username + "'"
    // );

    static AH::ServerDataVector const serverData {{ 1, 1, 1, true }};

    SEND_PACKET(*session, Af, *session);
    SEND_PACKET(*session, Ad, account.nickname);
    SEND_PACKET(*session, Ac, Ac::Community::FRENCH_SPEAKING);
    SEND_PACKET(*session, AH, serverData);
    SEND_PACKET(*session, Al, account.accessLevel > 0);
    SEND_PACKET(*session, AQ, account.question);
    return true;
}

DEFINE_LOGIN_SERVER_PACKET_HANDLER(Ax)
{
    // TODO: load amount of existing characters on server
    SEND_PACKET(*session, Ax, 0, *session);
    return true;
}

DEFINE_LOGIN_SERVER_PACKET_HANDLER(AF)
{
    AF::CharacterDistributionVector const distribution;
    // TODO: load from database
    SEND_PACKET(*session, AF, distribution);
    return true;
}

DEFINE_LOGIN_SERVER_PACKET_HANDLER(AX)
{
    // TODO: load host from config
    static constexpr auto const host { "127.0.0.1" };
    static constexpr auto const port { 4242        };

    if (packet.serverId != 1)
    {
        std::cerr << "Unknown server ID '" << packet.serverId << "' in server selection."
                     " Potential packet forgery or non-retail client!" << std::endl;
        SEND_PACKET_ERROR(*session, AX, AX::Error::UNSPECIFIED);
        return false;
    }

    auto & account = ACCOUNT(*session);

    std::ostringstream oss;
    oss << std::hex << session->id();
    account.connectionTicket = oss.str();

    SESSION_MANAGER.markSessionAsPending(session->id(), account.connectionTicket);

    static std::regex const ipRegExp { R"(\d{1,3}(\.\d{1,3}){3})" };
    if (std::regex_match(host, ipRegExp))
        SEND_PACKET(*session, AX, host, port, account.connectionTicket);
    else
        SEND_PACKET(*session, AY, host, port, account.connectionTicket);

    return true;
}
