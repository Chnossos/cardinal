#include <Cardinal/PacketDispatchers/LoginServerDispatcher.hpp>

#include <Cardinal/Network/SessionManager.hpp>
#include <Cardinal/Packets/Ax_CharacterCounts.hpp>
#include <Cardinal/Packets/AF_FriendCharacterCounts.hpp>
#include <Cardinal/Packets/AXY_ServerSelection.hpp>
#include <Cardinal/Packets/Af_WaitingQueue.hpp>
#include <iostream>

using PacketDispatchers::LoginServerDispatcher;

DEFINE_PACKET_FACTORY_MAP(LoginServerDispatcher,
    LOGIN_SERVER_DISPATCHER_FACTORY(Af),
    LOGIN_SERVER_DISPATCHER_FACTORY(AF),
    LOGIN_SERVER_DISPATCHER_FACTORY(Ax),
    LOGIN_SERVER_DISPATCHER_FACTORY(AX),
);

DEFINE_PACKET_HANDLER_MAP(LoginServerDispatcher,
    LOGIN_SERVER_DISPATCHER_HANDLER(Af),
    LOGIN_SERVER_DISPATCHER_HANDLER(AF),
    LOGIN_SERVER_DISPATCHER_HANDLER(Ax),
    LOGIN_SERVER_DISPATCHER_HANDLER(AX),
);

bool LoginServerDispatcher::dispatch(std::string_view packet, Network::Session::Ptr const & session)
{
    if (packet.size() < 2)
    {
        std::cerr << "Error: Packet is too small (content: <" << packet << ">). "
                  << "Potential packet forgery or non-retail client!" << std::endl;
        SESSION_MANAGER.removeSession(session->id());
        return false;
    }

    auto const header    = packet.substr(0, 2);
    auto const factoryIt = _packetFactory.find(header);
    auto const handlerIt = _packetHandlers.find(header);

    if (factoryIt != _packetFactory.cend() && handlerIt != _packetHandlers.cend())
    {
        APP.io_context.post([handlerIt, factoryIt, session, p = std::string(packet.substr(2))]
        {
            if (!handlerIt->second(*factoryIt->second(p), session))
                SESSION_MANAGER.removeSession(session->id());
            else
                session->read();
        });
        return true;
    }
    else
    {
        std::cerr << "Warning: Unknown or unsupported packet read "
                  << "(header: <" << header << ">)" << std::endl;
        SESSION_MANAGER.removeSession(session->id());
        return false;
    }
}
