#pragma once

#include <Cardinal/Network/Session.hpp>
#include <Cardinal/Utility/Component.hpp>
#include <memory>
#include <string_view>
#include <unordered_map>

namespace Network
{
class SessionManager : public Utility::Component
{
private:
    std::unordered_map<Session::Id, std::shared_ptr<Session>> _sessions;
    std::unordered_map<std::string_view, Session::Id>         _pendingSessions;

public:
    void shutdown();

public:
    void markSessionAsPending(Session::Id id, std::string_view connectionTicket);
    bool mergeSessionWithPending(Session & tmpSession, std::string_view ticket);
    void removeSession(Session::Id id);

public:
    void onNewSocketAccepted(boost::asio::ip::tcp::socket && socket);
};
} // !namespace Network

#define SESSION_MANAGER APP_COMPONENT(Network::SessionManager, "SessionManager")
