#pragma once

#include <Cardinal/Network/IPacket.hpp>

namespace Network::Packet
{
    class AQ : public BasicPacket<AQ>
    {
    public:
        std::string const & question;

    public:
        explicit AQ(std::string const & question_) : question { question_ } {}

    private:
        void serialize(std::ostream & writer) const override {
            writer << question;
        }
    };
} // !namespace Network::Packet

INIT_PACKET_HEADER(AQ);
