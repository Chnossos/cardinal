#pragma once

#include <Cardinal/Network/IPacket.hpp>
#include <Cardinal/Utility/Entity.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/streambuf.hpp>
#include <memory>
#include <string>

namespace Network
{
class Session : public std::enable_shared_from_this<Session>, public Utility::Entity
{
public:
    using Id  = int;
    using Ptr = std::shared_ptr<Session>;

private:
    Id const                     _id;
    boost::asio::ip::tcp::socket _socket;
    boost::asio::streambuf       _buffer;

public:
    Session(Id id, boost::asio::ip::tcp::socket && socket);

public:
    void start();
    void resumeFrom(Session & other);
    void shutdown();
    void send(std::string packet);
    void send(Network::IPacket const & packet);
    template<typename Packet, typename... Args>
    void send(Args &&... args);
    void read();

public:
    auto id() const -> Id          { return _id; }
    auto ip() const -> std::string { return _socket.remote_endpoint().address().to_string(); }

private:
    void onPacketSent(Ptr self, std::shared_ptr<std::string> packet,
                      std::size_t bytesTransferred, boost::system::error_code const & ec);
    void onPacketRead(Ptr self, boost::system::error_code const & ec);
    bool handleError(boost::system::error_code const & ec);
};

inline void Session::send(IPacket const & packet) {
    send(packet.serialize());
}

template<typename Packet, typename... Args>
inline void Session::send(Args &&... args) {
    send(Packet(std::forward<Args>(args)...));
}
} // !namespace Network

#define SEND_PACKET(s, P, ...)          (s).send<Network::Packet::P>(__VA_OPT__(__VA_ARGS__))
#define SEND_PACKET_ERROR(s, P, C, ...) (s).send<Network::Packet::P ## E<C>>(__VA_OPT__(__VA_ARGS__))
