#include <Cardinal/Network/Session.hpp>

#include <Cardinal/Application.hpp>
#include <Cardinal/Components/Account.hpp>
#include <Cardinal/PacketDispatchers/TicketDispatcher.hpp>
#include <Cardinal/Network/SessionManager.hpp>
#include <Cardinal/Packets/AT_AccountTicket.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/write.hpp>
#include <iostream>

using namespace Network;
using namespace Network::Packet;

Session::Session(Id id, boost::asio::ip::tcp::socket && socket)
    : _id     { id                }
    , _socket { std::move(socket) }
{
    addComponent<PacketDispatchers::TicketDispatcher>("PacketDispatcher");
}

void Session::start()
{
    send("HG");
    read();
}

void Session::resumeFrom(Session & other)
{
    _socket = std::move(other._socket);
    ACCOUNT(*this).connectionTicket.clear();
    SEND_PACKET(*this, AT, 0);
    read();
}

void Session::shutdown()
{
    boost::system::error_code ec;
    if (_socket.shutdown(_socket.shutdown_both, ec) || _socket.close(ec))
        handleError(ec);
}

void Session::send(std::string packet)
{
    using namespace std::placeholders;
    auto const packetPtr = std::make_shared<std::string>(std::move(packet += '\0'));
    boost::asio::async_write(_socket, boost::asio::buffer(*packetPtr),
        std::bind(&Session::onPacketSent, this, shared_from_this(), packetPtr, _2, _1)
    );
}

void Session::read()
{
    using namespace std::placeholders;
    boost::asio::async_read_until(_socket, _buffer, '\0',
        std::bind(&Session::onPacketRead, this, shared_from_this(), _1)
    );
}

void Session::onPacketSent(Session::Ptr, std::shared_ptr<std::string> packet,
                           std::size_t, boost::system::error_code const & ec)
{
    if (handleError(ec)) return ;

    packet->pop_back(); // Remove leading '\0' for display
    std::cout << "sent (id: " << _id << "): <" << *packet << '>' << std::endl;
}

void Session::onPacketRead(Ptr, boost::system::error_code const & ec)
{
    if (handleError(ec)) return ;

    std::string  packet;
    std::istream reader { &_buffer };
    std::getline(reader, packet, '\0');

    if (packet.empty() || packet.back() != '\n')
    {
        // uh-oh, something's fishy, get this MOFO outta here
        std::cerr << "You think this is a motherfucking game '" << _id << "'?" << std::endl;
        SESSION_MANAGER.removeSession(_id);
        return ;
    }

    packet.pop_back(); // Remove the leading '\n' character
    std::cout << "read (id: " << _id << "): <" << packet << '>' << std::endl;
    PACKET_DISPATCHER(*this).dispatch(packet, shared_from_this());
}

bool Session::handleError(boost::system::error_code const & ec)
{
    switch (auto const code = ec.default_error_condition().value())
    {
        case boost::system::errc::success:
            return false;

#if BOOST_OS_WINDOWS
        case ERROR_CONNECTION_ABORTED:
#endif
        case boost::system::errc::operation_canceled:        // server-side disconnection
        case boost::system::errc::no_such_file_or_directory: // client-side disconnection
            std::cout << "Session (id: " << _id << ") disconnected (code: " << code << ')' << std::endl;
            break;

        default:
            std::cerr << "Session (id: " << _id << ") encountered unknown error "
                      << code << ": " << ec.message() << std::endl;
            break;
    }
    if (_socket.is_open() && ACCOUNT(*this).connectionTicket.empty())
        SESSION_MANAGER.removeSession(_id);

    return true;
}
