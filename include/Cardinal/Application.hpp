#pragma once

#include <Cardinal/Utility/Entity.hpp>
#include <Cardinal/Utility/Singleton.hpp>
#include <boost/asio/io_context.hpp>
#include <string>
#include <vector>

class Application final : public Utility::Singleton<Application>, public Utility::Entity
{
    friend class Utility::Singleton<Application>;

public:
    std::vector<std::string> const args;
    boost::asio::io_context        io_context;

private:
    Application(int const & argc, char * argv[])
        : args { argv, argv + argc }
    {}

public:
    int  run();
    void shutdown();

private:
    void onSignal(boost::system::error_code const & error, int signal);
    void handleSignal(int signal);
};

#define APP Application::instance()
#define APP_COMPONENT(TYPE, NAME) APP.component<TYPE>(NAME)
