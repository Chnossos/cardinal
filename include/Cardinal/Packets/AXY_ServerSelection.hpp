#pragma once

#include <Cardinal/Network/IPacket.hpp>
#include <Cardinal/Utility/Join.hpp>
#include <vector>

namespace Network::Packet
{
namespace details
{
    class ServerSelectionBase
    {
    public:
        enum Error : char
        {
            UNSPECIFIED,
            ACCESS_DENIED              = 'r',
            SERVER_DOWN                = 'd',
            SERVER_FULL                = 'F',
            SERVER_FULL_SELECT_ANOTHER = 'f',
        };

    protected:
        std::string   const host;
        std::uint16_t const port = 0;
        std::string   const ticket;

    public:
        ServerSelectionBase() = default;
        ServerSelectionBase(std::string host_, std::uint16_t port_, std::string ticket_)
        : host   { std::move(host_)   }
        , port   { port_              }
        , ticket { std::move(ticket_) }
        {}
    };
} // !namespace details

    class AX : public details::ServerSelectionBase, public SuccessPacket<AX>
    {
    public:
        int const serverId = 0;

    public:
        using ServerSelectionBase::ServerSelectionBase;
        explicit AX(std::string_view const & rawData)
            : serverId { std::atoi(rawData.data()) } // NOLINT because we want 0
        {}                                           //        in case of failure

    private:
        void serialize(std::ostream & writer) const override;
    };

    DECLARE_BASE_ERROR_PACKET(AX) {};

    DECLARE_ERROR_PACKET(AX, AX::Error::SERVER_FULL_SELECT_ANOTHER)
    {
    public:
        using ServerIdVector = std::vector<int>;
        ServerIdVector const serversId;

    public:
        explicit AXE(ServerIdVector serversId_) : serversId { std::move(serversId_) } {}

    private:
        void serialize(std::ostream & writer) const override {
            Utility::join(writer, "|", std::cbegin(serversId), std::cend(serversId));
        }
    };

    /* *************************************************************************
     * Non-encrypted version
     * ************************************************************************/

    class AY : public details::ServerSelectionBase, public SuccessPacket<AY>
    {
    public:
        using ServerSelectionBase::ServerSelectionBase;

    private:
        void serialize(std::ostream & writer) const override;
    };

    DECLARE_BASE_ERROR_PACKET(AY) {};

    DECLARE_ERROR_PACKET(AY, AY::Error::SERVER_FULL_SELECT_ANOTHER)
    {
    public:
        using ServerIdVector = std::vector<int>;
        ServerIdVector const serversId;

    public:
        explicit AYE(ServerIdVector serversId_) : serversId { std::move(serversId_) } {}

    private:
        void serialize(std::ostream & writer) const override {
            Utility::join(writer, "|", std::cbegin(serversId), std::cend(serversId));
        }
    };
} // !namespace Network::Packet

INIT_PACKET_HEADER(AX);
INIT_PACKET_HEADER(AY);
