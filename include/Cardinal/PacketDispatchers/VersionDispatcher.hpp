#pragma once

#include <Cardinal/Components/PacketDispatcher.hpp>

namespace PacketDispatchers
{
    struct VersionDispatcher : public Components::PacketDispatcher<VersionDispatcher>
    {
        bool dispatch(std::string_view packet, Network::Session::Ptr const & s) final;
    };
} // !namespace PacketDispatchers
