#pragma once

#include <ostream>

namespace Utility
{
    struct Serializable
    {
        virtual ~Serializable() = 0;
        virtual std::ostream & serialize(std::ostream & os) const { return os; }
    };

    inline Serializable::~Serializable() = default;
} // !namespace Utility

inline std::ostream & operator<<(std::ostream & os, Utility::Serializable const & s)
{
    return s.serialize(os);
}

template<typename T>
inline std::ostream & operator<<(std::ostream & os,
    std::enable_if_t<std::is_base_of_v<Utility::Serializable, T>, T> const & t)
{
    return os << static_cast<Utility::Serializable const &>(t);
}
