DROP TABLE IF EXISTS accounts;
CREATE TABLE accounts
(
    id                    SERIAL      PRIMARY KEY,
    name                  VARCHAR(30) NOT NULL UNIQUE KEY,
    password              TINYTEXT    NOT NULL,
    nickname              VARCHAR(30) NOT NULL UNIQUE KEY,
    access_level          TINYINT     NOT NULL DEFAULT 0,
    question              TEXT        NOT NULL,
    answer                TEXT        NOT NULL,
    creation_date         TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    subscription_end_date DATETIME,
    banishment_end_date   DATETIME,
    last_connection_date  DATETIME,
    last_ip               TINYTEXT
);

INSERT INTO accounts (name, password, nickname, access_level, question, answer) VALUES
('Admin', 'admin', 'Administrator', 1, 'DELETE?', 'DELETE'); /* 21232f297a57a5a743894a0e4a801fc3 */