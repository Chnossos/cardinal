#pragma once

#include <Cardinal/Network/IPacket.hpp>
#include <Cardinal/Network/Session.hpp>
#include <string>
#include <string_view>

namespace Network::Packet
{
    class AT : public SuccessPacket<AT>
    {
    public:
        std::string const connectionKey;
        int         const encryptionLevel = 0;

    public:
        explicit AT(std::string_view const & data)
            : connectionKey { data }
        {}
        explicit AT(int encryptionLevel_)
            : encryptionLevel { encryptionLevel_ }
        {}

    private:
        void serialize(std::ostream & writer) const override {
            writer << encryptionLevel;
        }
    };
} // !namespace Network::Packet

INIT_PACKET_HEADER(AT);
