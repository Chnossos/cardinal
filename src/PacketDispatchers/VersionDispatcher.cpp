#include <Cardinal/PacketDispatchers/VersionDispatcher.hpp>

#include <Cardinal/PacketDispatchers/CredentialsDispatcher.hpp>
#include <Cardinal/Network/SessionManager.hpp>
#include <Cardinal/Packets/Al_AccountLoginResult.hpp>

#include <regex>

using namespace PacketDispatchers;
using namespace Network::Packet;

DEFINE_PACKET_FACTORY_MAP(VersionDispatcher);
DEFINE_PACKET_HANDLER_MAP(VersionDispatcher);

bool VersionDispatcher::dispatch(std::string_view packet, Network::Session::Ptr const & session)
{
    std::regex const validator { R"(1\.(\d+)(\.\d+)?e?)" };

    std::smatch results;
    std::string const p { packet };
    if (std::regex_match(p, results, validator))
    {
        auto const minor = std::stoi(results[1]);
        if (minor > 29 || (minor == 29 && results[2].compare(".1") == 0))
        {
            session->replaceComponent<CredentialsDispatcher>("PacketDispatcher");
            session->read();
            return true;
        }
    }
    SEND_PACKET_ERROR(*session, Al, Al::Error::BAD_VERSION, "Retro");
    SESSION_MANAGER.removeSession(session->id());
    return false;
}
