#pragma once

// #include <Cardinal/MySQL/Row.hpp>
#include <Cardinal/Utility/Component.hpp>
#include <ctime>
#include <string>

namespace Components
{

struct Account : public Utility::Component
{
    std::string username, password;
    std::string question, answer;
    std::string nickname;
    std::string connectionKey, connectionTicket;
    std::time_t remainingSubscriptionTime = 0;
    std::time_t remainingBanTime = 0;
    std::int8_t accessLevel = 0;
    std::time_t lastConnectionDate = 0;
    std::string lastIp;
};

} // !namespace Components

// inline bool operator>>(Sql::Row const & r, Components::Account & a)
// {
//     return r.deserialize(a.nickname, a.accessLevel, a.question, a.answer,
//         a.remainingSubscriptionTime, a.remainingBanTime, a.lastConnectionDate, a.lastIp);
// }

#define ACCOUNT(s) (s).component<Components::Account>("Account")
