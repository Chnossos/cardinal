#pragma once

#include <Cardinal/Utility/Component.hpp>
#include <memory>
#include <string>
#include <unordered_map>

namespace Utility
{
class Entity
{
private:
    std::unordered_map<std::string, std::unique_ptr<Component>> _components;

public:
    template<typename T, typename... Args>
    auto addComponent(std::string const & name, Args &&... args) -> T &
    {
        static_assert(std::is_base_of_v<Component, T>, "T must inherit Utility::Component");
        auto const component = new T { std::forward<Args>(args)... };
        return static_cast<T &>(*_components.emplace(name, component).first->second);
    }

    template<typename T, typename... Args>
    auto replaceComponent(std::string const & name, Args &&... args) -> T &
    {
        delComponent(name);
        return addComponent<T>(name, std::forward<Args>(args)...);
    }

    void delComponent(std::string const & name) {
        _components.erase(name);
    }

    bool hasComponent(std::string const & name) const {
        return _components.count(name) > 0;
    }

    template<typename T>
    auto component(std::string const & name) -> T & {
        return static_cast<T &>(*_components.at(name));
    }

    template <typename T>
    auto component(std::string const & name) const -> T const & {
        return static_cast<T const &>(*_components.at(name));
    }
};
} // !namespace Utility
