#include <Cardinal/Packets/AXY_ServerSelection.hpp>

#include <array>

using namespace Network::Packet;

namespace
{
    std::string encryptHostInfo(std::string_view host, std::uint16_t port)
    {
        static constexpr auto const hash = "abcdefghijklmnopqrstuvwxyz"
                                           "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                           "0123456789-_";

        char dot;
        std::array<int, 4> parts {};
        std::istringstream iss { host.data() };
        iss >> parts[0] >> dot >> parts[1] >> dot >> parts[2] >> dot >> parts[3];

        std::string result;
        result.reserve(parts.size() * 2 + 3);

        for (auto const part : parts)
        {
            result += static_cast<char>('0' + (part >> 4 & 15));
            result += static_cast<char>('0' + (part      & 15));
        }
        result += hash[port >> 12 & 63];
        result += hash[port >>  6 & 63];
        result += hash[port       & 63];

        return result;
    }
} // !namespace

void AX::serialize(std::ostream & writer) const
{
    static std::string const encryptedHostInfo = encryptHostInfo(host, port);
    writer << encryptedHostInfo << ticket;
}

void AY::serialize(std::ostream & writer) const
{
    writer << host << ':' << port << ';' << ticket;
}

