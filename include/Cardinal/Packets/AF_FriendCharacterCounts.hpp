#pragma once

#include <Cardinal/Network/IPacket.hpp>
#include <Cardinal/Utility/Join.hpp>
#include <Cardinal/Utility/Serializable.hpp>
#include <vector>

namespace Network::Packet
{
    class AF : public BasicPacket<AF>
    {
    private:
        struct CharacterDistribution : public Utility::Serializable
        {
            int const serverId, characterCount;
            CharacterDistribution(int serverId_, int characterCount_)
                : serverId       { serverId_       }
                , characterCount { characterCount_ }
            {}
            std::ostream & serialize(std::ostream & writer) const override;
        };

    public:
        using CharacterDistributionVector = std::vector<CharacterDistribution>;
        CharacterDistributionVector const characterDistribution;

    public:
        explicit AF(std::string_view) {}
        explicit AF(CharacterDistributionVector characterDistribution_)
            : characterDistribution { std::move(characterDistribution_) }
        {}

    private:
        void serialize(std::ostream & writer) const override;
    };
} // !namespace Network::Packet

INIT_PACKET_HEADER(AF);
