#pragma once

#include <ostream>
#include <string_view>
#include <type_traits>

namespace Utility
{
namespace details
{
    template<typename FirstArg, typename... Args>
    std::ostream & join(std::ostream & os, std::string_view const & separator,
                        FirstArg && firstArg, Args &&... args)
    {
        os << std::forward<FirstArg>(firstArg);
        ((os << separator << std::forward<Args>(args)), ...);
        return os;
    }
} // !namespace details

// Enabled only if (*Iterator != Iterator)
template<typename Iterator, typename = std::enable_if_t<
    !std::is_same_v<decltype(*std::declval<Iterator>()), Iterator>>>
std::ostream & join(std::ostream & os, std::string_view separator,
                    Iterator begin, Iterator end)
{
    if (begin != end)
    {
        os << *begin;
        while (++begin != end)
            os << separator << *begin;
    }
    return os;
}

template<typename Iterator, typename = std::enable_if_t<
    !std::is_same_v<decltype(*std::declval<Iterator>()), Iterator>>>
std::ostream & join(std::ostream & os, char separator, Iterator begin, Iterator end)
{
    std::string_view v { &separator, 1 };
    return join(os, v, begin, end);
}

template<typename... Args>
std::ostream & join(std::ostream & os, std::string_view separator, Args &&... args)
{
    return details::join(os, separator, std::forward<Args>(args)...);
}

template<typename... Args>
std::ostream & join(std::ostream & os, char separator, Args &&... args)
{
    std::string_view v { &separator, 1 };
    return details::join(os, v, std::forward<Args>(args)...);
}
} // !namespace Utility
