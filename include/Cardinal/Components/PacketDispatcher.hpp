#pragma once

#include <Cardinal/Application.hpp>
#include <Cardinal/Network/IPacket.hpp>
#include <Cardinal/Network/Session.hpp>
#include <Cardinal/Utility/Component.hpp>
#include <functional>
#include <memory>
#include <string>
#include <string_view>

namespace Components
{
class IPacketDispatcher : public Utility::Component
{
public:
    virtual bool dispatch(std::string_view packet, Network::Session::Ptr const & s) = 0;

public:
    template<typename T>
    static std::unique_ptr<Network::IPacket> make(std::string_view packet)
    {
        static_assert(std::is_base_of_v<Network::IPacket, T>, "T must inherit IPacket!");
        return std::make_unique<T>(packet);
    }
};

namespace details
{
    template<typename T>
    class PacketDispatcherImpl : public IPacketDispatcher
    {
        using PacketFactory = std::unique_ptr<Network::IPacket> (*)(std::string_view);
        using PacketHandler = bool (*)(Network::IPacket const &, Network::Session::Ptr const &);

    public:
        using PacketHandlerMap = std::unordered_map<std::string_view, PacketHandler>;
        using PacketFactoryMap = std::unordered_map<std::string_view, PacketFactory>;

    protected:
        static PacketHandlerMap const _packetHandlers;
        static PacketFactoryMap const _packetFactory;

    private:
        template<typename Packet>
        static bool callHandler(Network::IPacket      const & packet,
                                Network::Session::Ptr const & session)
        {
            static_assert(std::is_base_of_v<Network::IPacket, Packet>, "P must inherit IPacket!");
            return handlePacket(static_cast<Packet const &>(packet), session);
        }

        template<typename Packet>
        static bool handlePacket(Packet const &, Network::Session::Ptr const & session);
    };
} // !namespace details

template<typename T>
using PacketDispatcher = details::PacketDispatcherImpl<T>;
} // !namespace Components

#define PACKET_DISPATCHER(e) (e).component<Components::IPacketDispatcher>("PacketDispatcher")

#define DEFINE_PACKET_FACTORY_MAP(D, ...) \
    template<> \
    Components::details::PacketDispatcherImpl<PacketDispatchers::D>::PacketFactoryMap const \
    Components::details::PacketDispatcherImpl<PacketDispatchers::D>::_packetFactory \
    __VA_OPT__(= { __VA_ARGS__ })

#define DEFINE_PACKET_HANDLER_MAP(D, ...) \
    template<> \
    Components::details::PacketDispatcherImpl<PacketDispatchers::D>::PacketHandlerMap const \
    Components::details::PacketDispatcherImpl<PacketDispatchers::D>::_packetHandlers \
    __VA_OPT__(= { __VA_ARGS__ })

#define CUSTOM_FACTORY(D, H, P) { H, &PacketDispatchers::D::make<Network::Packet::P> }
#define DEFAULT_FACTORY(D, P) CUSTOM_FACTORY(D, Network::Packet::P::header, P)

#define CUSTOM_HANDLER(D, H, P) \
    { H, &Components::details::PacketDispatcherImpl<PacketDispatchers::D>\
            ::callHandler<Network::Packet::P> }
#define DEFAULT_HANDLER(D, P) CUSTOM_HANDLER(D, Network::Packet::P::header, P)

#define DEFINE_PACKET_HANDLER(D, P) \
    template<> \
    template<> bool \
    Components::details::PacketDispatcherImpl<PacketDispatchers::D>::handlePacket<>( \
        [[maybe_unused]] Network::Packet::P    const & packet, \
        [[maybe_unused]] Network::Session::Ptr const & session)
