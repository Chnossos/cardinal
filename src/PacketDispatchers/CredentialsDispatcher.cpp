#include <Cardinal/PacketDispatchers/CredentialsDispatcher.hpp>

#include <Cardinal/Components/Account.hpp>
#include <Cardinal/PacketDispatchers/LoginServerDispatcher.hpp>
#include <Cardinal/Network/SessionManager.hpp>
#include <Cardinal/Packets/Al_AccountLoginResult.hpp>
#include <Cardinal/Packets/Password.hpp>

using namespace PacketDispatchers;
using namespace Network::Packet;

DEFINE_PACKET_FACTORY_MAP(CredentialsDispatcher,
    DEFAULT_FACTORY(CredentialsDispatcher, Password)
);

DEFINE_PACKET_HANDLER_MAP(CredentialsDispatcher,
    DEFAULT_HANDLER(CredentialsDispatcher, Password)
);

bool CredentialsDispatcher::dispatch(std::string_view packet, Network::Session::Ptr const & session)
{
    auto const pos = packet.find('\n');
    // Packet must have a separator and be long enough to read a password (header + actual password)
    if (pos && pos != std::string::npos && packet.size() >= pos + 4)
    {
        ACCOUNT(*session).username = packet.substr(0, pos);
        auto const password = packet.substr(pos + 1);
        if (password.compare(0, 2, Password::header) == 0)
        {
            auto const packetPtr = _packetFactory.at(Password::header)(password.substr(2));
            if (_packetHandlers.at(Password::header)(*packetPtr, session))
            {
                session->replaceComponent<LoginServerDispatcher>("PacketDispatcher");
                session->read();
                return true;
            }
        }
        else
            SEND_PACKET_ERROR(*session, Al, Al::Error::ACCESS_DENIED);
    }
    SESSION_MANAGER.removeSession(session->id());
    return false;
}
