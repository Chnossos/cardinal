#pragma once

#include <Cardinal/Utility/Component.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <functional>
#include <unordered_map>

namespace Network
{
class SocketListener : public Utility::Component
{
    using AcceptCallback = std::function<void (boost::asio::ip::tcp::socket &&)>;
    using TCPAcceptor    = boost::asio::ip::tcp::acceptor;

private:
    boost::asio::io_context &                    _io_context;
    std::unordered_map<uint16_t, TCPAcceptor>    _acceptors;
    std::unordered_map<uint16_t, AcceptCallback> _callbacks;

public:
    SocketListener(boost::asio::io_context & io_context);

public:
    int listen(uint16_t port, AcceptCallback cb);
    void shutdown();

private:
    void onAccept(uint16_t port,
                  boost::system::error_code const & error,
                  boost::asio::ip::tcp::socket socket);
    int handleError(uint16_t port, boost::system::error_code const & ec);
};
} // !namespace Network

#define SOCKET_LISTENER APP_COMPONENT(Network::SocketListener, "SocketListener")
