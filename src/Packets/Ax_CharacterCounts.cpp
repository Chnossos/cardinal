#include <Cardinal/Packets/Ax_CharacterCounts.hpp>

#ifdef NDEBUG
# include <Cardinal/Components/Account.hpp>
#endif

using Network::Packet::Ax;

Ax::Ax(int characterAmount, [[maybe_unused]] Network::Session const & session):
#ifdef NDEBUG
    remainingSubscriptionTime { ACCOUNT(session).remainingSubscriptionTime },
#endif
    characterAmountPerServer {{ 1, characterAmount }}
{}

void Ax::serialize(std::ostream & writer) const
{
    writer << remainingSubscriptionTime
           << (remainingSubscriptionTime > 0 ? "000" : "");

    for (auto && [serverId, amount] : characterAmountPerServer)
        writer << '|' << serverId << ',' << amount;
}
