#include <Cardinal/Packets/Af_WaitingQueue.hpp>

#include <Cardinal/Utility/Join.hpp>

using Network::Packet::Af;

void Af::serialize(std::ostream & writer) const
{
    Utility::join(writer, '|',
        position, totalSubscribers, totalNonSubscribers, isSubscriber, queueId);
}
